

******

# Medical District DB


**literature**
```literature
A DB that exists while a browser page is open.
It doesn't interface with cookies, localStorage, etc. 
```

******

```tcsh
[tcsh] yarn add @medical-district/db
```

******

**javascript**
```javascript
import { MAKE_DB } from './../index.js'
```

```javascript

/*
	Function series of functions that are only called once
	after MAKE_DB is called.

		STORAGE
		AT.START
*/
const DB = await MAKE_DB ({
	STORAGE: async function () {
		console.log ("STORAGE function called")
		
		return {
			STATUS: 7
		}
	},

	PLAYS: {
		async ["multiply 'STATUS' by absolute of number >= 2"] (
			{ CHANGE, STORAGE }, 
			{ NUMBER }
		) {
			console.log (`"multiply 'STATUS' by absolute of number >= 2" function called`, { NUMBER })

			let MULTIPLICAND = (await STORAGE ()).STATUS
			let MULTIPLICATOR = Math.abs (NUMBER)
			if (MULTIPLICATOR < 2) {
				return "unsuccessful";
			}
			
			await CHANGE ("STATUS", MULTIPLICAND * MULTIPLICATOR)
			
			return "successful";
		}
	},
	
	AT: {
		async START ({ PLAYS, STORAGE }) {
			console.log ("START function called")
			
			// await PLAYS.PLAY_1 ({ REPEAT: 1 })
		}
	}			
})	

/*
	DB Subscriptions:
	
	Notes:
		The subscribe function is called every time
		a play calls "CHANGE", such as:
		
			await CHANGE ("STATUS", 998)
*/		
const UNSUBSCRIBE = DB.SUBSCRIBE (({ FIRST, FIELD }) => {
	const STORAGE = DB.STORAGE ()

	console.log ('SUBSCRIBE function', { FIRST, FIELD, STORAGE })
})

/*
	After subscribing, the subscriber count is 1
*/
assert.equal (DB.SUBSCRIBER_COUNT (), 1)


/*
	Plays
*/
const PLAY_1_AFTERMATH = await DB.PLAYS ["multiply 'STATUS' by absolute of number >= 2"] ({ NUMBER: 10 })
const PLAY_2_AFTERMATH = await DB.PLAYS ["multiply 'STATUS' by absolute of number >= 2"] ({ NUMBER: 998 })

// equals "successful"
console.log ("PLAY_1_AFTERMATH:", PLAY_1_AFTERMATH) 

// equals "unsuccessful"	
console.log ("PLAY_2_AFTERMATH:", PLAY_2_AFTERMATH)

	

UNSUBSCRIBE ()

/*
	After unsubscribing, the subscriber count
*/
assert.equal (DB.SUBSCRIBER_COUNT (), 0)
```

******

## Vue usage


### '@/warehouses/status-db/index.js'
```javascript

export let STATUS_DB;

export const CREATE_STATUS_DB = async function () {
	const DB = await MAKE_DB ()
})
```

******


### '@/decor/item 1.vue'
```javascript
import { STATUS_DB } from '@/warehouses/status-db'
	
	
/*
	SEQUENCE:
		beforeCreate
		data
		created
		beforeMount
		mounted
		
		beforeUnmount
		unmounted
*/
export default {
	data () {
		return {
			STATUS: STATUS_DB.STORAGE ("STATUS")
		}
	},
	created () {		
		this.STATUS_DB_UNSUBSCRIBE 	= STATUS_DB.SUBSCRIBE (({ FIRST }) => {
			this.STATUS = STATUS_DB.STORAGE ("STATUS")
		})		
	},
	beforeUnmount () {
		this.STATUS_DB_UNSUBSCRIBE ()
	}
}
```

******
