



import cloneDeep 			from 'lodash/cloneDeep'

import { change_occurred } 	from './prototypes/change_occurred'
import { stop_monitoring } 	from './prototypes/stop_monitoring'
import { controls } 		from './prototypes/controls'

const HAS_field = function (this_object, field) {	
	if (Object.prototype.hasOwnProperty.call (this_object, field)) {
		return true;
	}
	
	return false;
}

function system () {
	this.monitors = []	
	this.warehouse = {};
	this.once_at = {}
	
	this.film = 0;
}
system.prototype.controls = controls
system.prototype.change_occurred = change_occurred
system.prototype.stop_monitoring = stop_monitoring

export async function make_system (original_params) {
	const this_system = new system ();
	const these_controls = this_system.controls ();
	
	async function make (PARAMS) {
		if (this_system.film >= 1) { console.log ("system: received params", this_system.PARAMS) }

		these_controls.activities = {}
		
		let fieldS = [];
		try {
			let fieldS = Object.keys (PARAMS)
		}	
		catch (EXCEPTION) {
			throw new Error (EXCEPTION)
		}
				
		for (const PARAM in PARAMS) {		
			if (PARAM === "warehouse") {
				this_system.warehouse = await PARAMS [ PARAM ] ()
			}
			
			if (PARAM === "activities") {				
				const activities = PARAMS [ PARAM ]
				for (const $circuit in activities) {
					if (this_system.film >= 1) { console.log ("system: ADDING $circuit", $circuit) }
					
					these_controls.activities [ $circuit ] = async function (OUTER_PARAMS) {
						if (this_system.film >= 1) { 
							console.log (`system: $circuit "${ $circuit }" CALLED`) 
						}
												
						return await activities [ $circuit ] ({
							change: these_controls.change,
							warehouse: async function (field) {
								if (this_system.film >= 1) { console.log ("system: warehouse FROM $circuit") }
								
								const warehouse = cloneDeep (this_system.warehouse)
																
								if (field === undefined) {
									return warehouse
								}
								
								return warehouse [ field ]
							},
							activities: these_controls.activities
						}, OUTER_PARAMS)
					}
				}
			}
			
			if (PARAM === "once_at") {
				this_system.once_at = PARAMS [ PARAM ]
			}
		}
				
		if (HAS_field (this_system.once_at, "start")) {
			return await this_system.once_at.start ({
				warehouse: async function (field) {
					if (this_system.film >= 1) { console.log ("system: warehouse from @start") }
					
					const warehouse = cloneDeep (this_system.warehouse)
								
					if (field === undefined) {
						return warehouse
					}
					
					return warehouse [ field ]
				},
				activities: these_controls.activities
			})
		}
		
		
	}	
	
	await make (original_params);
	
	return these_controls;
}
