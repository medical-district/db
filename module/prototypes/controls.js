

import cloneDeep 			from 'lodash/cloneDeep'

export function controls (PARAMS) {
	const this_system = this;

	function controls () {
		this.change = async function (field, VALUE) {
			if (this_system.film >= 1) { 
				console.log ("system: controls change FUNCTION CALLED", { field, VALUE }) 
			}
					
			this_system.warehouse [ field ] = VALUE;
					
			await this_system.change_occurred ({ field })
			return
		}
	}
	controls.prototype.warehouse = function (fieldS) {
		if (typeof fieldS === "string") {
			return cloneDeep (this_system.warehouse [ fieldS ])
		}
		
		return cloneDeep (this_system.warehouse)
	}
	
	/*
		{ monitor, glimpse, observe }
	*/
	controls.prototype.monitor = function (FN) {
		if (this_system.film >= 1) { console.log ("system: monitor was called") }
		
		this_system.monitors.push (FN)
		
		FN ({
			original: true
		})
		
		return {
			stop: function () {
				this_system.stop_monitoring (FN)
			}
		}		
	}
	controls.prototype.monitor_count = function (FN) {
		return this_system.monitors.length
	}	
	
	const I = new controls ()
	
	return I;
}