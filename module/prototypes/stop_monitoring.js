


export async function stop_monitoring (fn) {
	const monitors = this.monitors;
	
	for (let S = 0; S < monitors.length; S++) {
		const monitor = monitors [S]
		if (monitor === fn) {
			this.monitors.splice (S, 1)
			return;
		}		
	}
	
	console.error ("stop_monitoring WAS NOT POSSIBLE")
}