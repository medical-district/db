



/*
	await WAIT (1000)	
*/
const WAIT = async function (DURATION) {
	await new Promise (F => {
		setTimeout (() => {
			F ()
		}, DURATION)
	})
}


import { make_system } from './../index.js'
	
import { describe, it, expect } from 'vitest'
import assert from 'assert'

describe ('system', () => {
	it ('functions', async () => {
		let NOTIFICATIONS = 0;
		
		const system = await make_system ({
			warehouse: async function () {
				return {}
			},
			once_at: {
				async start () {}
			},
			activities: {}			
		})		
		
		const monitor = system.monitor (function ({ original }) {
			const warehouse = system.warehouse ()
			
			console.log ("monitor WAS CALLED", original, warehouse)
			
			NOTIFICATIONS += 1;
		})	
		assert.equal (system.monitor_count (), 1)
	
	
		const warehouse = system.warehouse ()
		assert.deepEqual (warehouse, {})

		monitor.stop ()
		assert.equal (system.monitor_count (), 0)
	})
		
	it ('can be customized', async () => {
		let NOTIFICATIONS = 0;
		let startED_CALLED = 0;
		
		const system = await make_system ({
			
			/*
				THE warehouse IS THE original THING THAT 
				IS BUILT WHEN THE system IS BEING MADE.
			*/
			warehouse: async function () {
				return {
					S: 1
				}
			},

			activities: {
				async circuit_1 ({ change, warehouse }, { REPEAT }) {	
					console.log ("CALLING PLAY 1", "REPEAT", REPEAT, await warehouse ())
	
					await WAIT (500)
					console.log ("CALLING PLAY 1 AFTER WAIT")
					
					let S = (await warehouse ()).S
					console.log ({ S })
					
					assert.deepEqual (await warehouse (),	{ S: REPEAT  })
					await change ("S", S + 1)
					assert.deepEqual (await warehouse (),	{ S: 1 + REPEAT })
					
					// await WAIT (500)
					
					console.log ("CALLING PLAY 1 AFTER change", { warehouse })
				}
			},
			
			once_at: {
				async start ({ activities, warehouse }) {
					assert.deepEqual (await warehouse (), { S: 1 })
					
					await WAIT (500)
					
					console.log ("startED", { activities })
					
					assert.deepEqual (await warehouse (), { S: 1 })
					await activities.circuit_1 ({ REPEAT: 1 })
					assert.deepEqual (await warehouse (), { S: 2 })
					
					startED_CALLED++;
				}
			}			
		})	

		assert.equal (startED_CALLED, 1)	
		
		assert.equal (NOTIFICATIONS, 0)
		
		//
		//	monitor TO THE system
		//		
		const monitor = system.monitor (function () {
			const warehouse = system.warehouse ()
			console.log ("monitor WAS CALLED", warehouse)
			NOTIFICATIONS += 1;
		})	
		
		// monitor FUNCTION IS CALLED ONCE ON INVOKATION
		assert.equal (NOTIFICATIONS, 1)
		
		assert.equal (system.monitor_count (), 	1)
		assert.deepEqual (system.warehouse (), 			{ S: 2 })

		
		//
		//	start a circuit
		//
		await system.activities.circuit_1 ({ REPEAT: 2 })
		assert.deepEqual (system.warehouse (), { S: 3 })
		
		//
		// monitor FUNCTION IS CALLED EVERY TIME A PLAY IS MAKE
		//
		assert.equal (NOTIFICATIONS, 2)
	

		monitor.stop ()
		assert.equal (system.monitor_count (), 0)
	})
})