

/*
	yarn run vitest status/2.test.js
*/


/*
	await WAIT (1000)	
*/
const WAIT = async function (DURATION) {
	await new Promise (F => {
		setTimeout (() => {
			F ()
		}, DURATION)
	})
}

import { make_system } from './../index.js'
	
import { describe, it, expect } from 'vitest'
import assert from 'assert'

describe ('system', () => {
	it ('functions', async () => {
		
		/*
			These functions are called only once,
			in this sequence, after "make_system" is called:
			
				warehouse
				once_at.start
		*/
		const system = await make_system ({
			warehouse: async function () {
				console.log ("warehouse function called")
				
				return {
					status: 7
				}
			},

			activities: {
				async ["multiply 'status' by absolute of number >= 2"] (
					{ change, warehouse }, 
					{ numeral }
				) {
					console.log (`"multiply 'status' by absolute of number >= 2" function called`, { numeral })

					let multiplicand = (await warehouse ()).status
					let multiplicator = Math.abs (numeral)
					if (multiplicator < 2) {
						return "unsuccessful";
					}
					
					await change ("status", multiplicand * multiplicator)
					
					return "successful";
				}
			},
			
			once_at: {
				async start ({ activities, warehouse }) {
					console.log ("start function called")					
				}
			}			
		})	

		/*
			system monitoring:
			
			notes:
				The monitor function is called once when
				setup, with "original" == true.
			
				The monitor function is called every time
				an activity calls "change", such as:
				
					await change ("status", 998)
		*/		
		const monitor = system.monitor (({ original, field }) => {
			const warehouse = system.warehouse ()

			console.log ('monitor function', { original, field, warehouse })
		})

		/*
			After subscribing, the subscriber count is 1
		*/
		assert.equal (system.monitor_count (), 1)


		/*
			activities
		*/
		const activity_aftermath_1 = await system.activities ["multiply 'status' by absolute of number >= 2"] ({ numeral: 10 })
		const activity_aftermath_2 = await system.activities ["multiply 'status' by absolute of number >= 2"] ({ numeral: 998 })
		
		// equals "successful"
		console.log ("activity_aftermath_1:", activity_aftermath_1) 
		
		// equals "unsuccessful"	
		console.log ("activity_aftermath_2:", activity_aftermath_2)

		monitor.stop ()

		/*
			After unsubscribing, the subscriber count
		*/
		assert.equal (system.monitor_count (), 0)
	})
})